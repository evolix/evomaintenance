# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant::DEFAULT_SERVER_URL.replace('https://vagrantcloud.com')

# Load ~/.VagrantFile if exist, permit local config provider
vagrantfile = File.join("#{Dir.home}", '.VagrantFile')
load File.expand_path(vagrantfile) if File.exists?(vagrantfile)

Vagrant.configure('2') do |config|
  config.vm.synced_folder "./", "/vagrant", type: "rsync", rsync__exclude: [ '.vagrant', '.git' ]
  config.ssh.shell = "/bin/sh"

  $deps = <<SCRIPT
DEBIAN_FRONTEND=noninteractive apt-get -yq install postgresql-client sudo sendmail
SCRIPT

  $install = <<SCRIPT
if [ ! -d /usr/share/scripts ]; then
	mkdir /usr/share/scripts
	chmod 700 /usr/share/scripts
	chown root:$1 /usr/share/scripts
fi
ln -fs /vagrant/evomaintenance.sh /usr/share/scripts
ln -fs /vagrant/evomaintenance.cf /etc
SCRIPT

  $trap = <<SCRIPT
trap_cmd='trap "sudo /usr/share/scripts/evomaintenance.sh" 0'
if [ -f /home/vagrant/.bash_profile ]; then
	if ! grep -q "$trap_cmd" /home/vagrant/.bash_profile; then
		echo "$trap_cmd" >> /home/vagrant/.bash_profile
	fi
elif [ -f /home/vagrant/.profile ]; then
	if ! grep -q "$trap_cmd" /home/vagrant/.profile; then
		echo "$trap_cmd" >> /home/vagrant/.profile
	fi
else
	echo "$trap_cmd" > /home/vagrant/.profile
fi
SCRIPT

  nodes = [
    { :name => "debian", :box => "debian/stretch64", :group => "root" },
    { :name => "openbsd", :box => "generic/openbsd6", :group => "wheel" }
  ]

  nodes.each do |i|
    config.vm.define "#{i[:name]}" do |node|
      node.vm.hostname = "evomaintenance-#{i[:name]}"
      node.vm.box = "#{i[:box]}"

      config.vm.provision "deps", type: "shell", :inline => $deps if "#{i[:name]}" == "debian"
      config.vm.provision "deps", type: "shell", :inline => "pkg_add postgresql-client-10.5p1" if "#{i[:name]}" == "openbsd"
      config.vm.provision "install", type: "shell", :inline => $install, :args => ["#{i[:group]}"]
      config.vm.provision "trap", type: "shell", :inline => $trap
    end
  end

end
