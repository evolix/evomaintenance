#!/bin/bash

#######################################################################
#                      Post-release script
#
# It must me executed after a release,
# to copy parts to the correct downstream projects.
#######################################################################


# Exit  immediately  if a pipeline (which may consist of a single simple command),
# a list, or a compound command (see SHELL GRAMMAR above), exits with a non-zero
# status.
set -o errexit
# If expansion is attempted on an unset variable or parameter, the shell prints an
# error message, and, if not interactive, exits with a non-zero status.
set -o nounset
# The pipeline's return status is the value of the last (rightmost) command
# to exit with a non-zero status, or zero if all commands exit successfully.
set -o pipefail
# Enable trace mode if called with environment variable TRACE=1
if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace
fi

REPOSITORIES_PARENT_DIR="${HOME}/GIT"

echo "Copy to ansible-roles.git :"

dest="${REPOSITORIES_PARENT_DIR}/ansible-roles/evomaintenance/files/upstream"

if [ ! -d "${dest}" ]; then
    echo "Target doesn't look like an evomaintenance upstream directory. Check before running again."
else
    cp CHANGELOG.md README.md evomaintenance.sh "${dest}/" && echo "Done!"
fi

echo "Copy to EvoBSD.git :"

dest="${REPOSITORIES_PARENT_DIR}/EvoBSD/roles/evomaintenance/files/upstream"

if [ ! -d "${dest}" ]; then
    echo "Target doesn't look like an evomaintenance upstream directory. Check before running again."
else
    cp CHANGELOG.md README.md evomaintenance.sh "${dest}/" && echo "Done!"
fi
